const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

const MAX_GROUP_SIZE = 50;

exports.getTournamentGroup = functions.https.onRequest((req, res) => {
  const tournamentId = String(req.query.tournamentId);
  const league = Number(req.query.league);
  const playerLevel = Number(req.query.level);
  admin.database().ref('TournamentsData/' + tournamentId + '/' + league + '/' + playerLevel)
                  .once('value', snapshot => {
                    var lastGroup = 0;
                    var needToIncrementGroup = true;
                    snapshot.forEach(childSnapshot => {
                      lastGroup = Number(childSnapshot.key);
                      if (childSnapshot.numChildren() < MAX_GROUP_SIZE) {
                        needToIncrementGroup = false;
                        return true; //cancel enumeration
                      } else {
                        return false;
                      }
                    })
                    if (needToIncrementGroup) {
                      lastGroup = lastGroup + 1;
                    }
                    res.json({"group": String(playerLevel) + '/' + lastGroup});
                  });
});

exports.createTournament = functions.https.onRequest((req, res) => {
  const secretKey = 'Afx0s';
  const keyIsCorrect = secretKey === (String(req.query.key) || '');
  const tournamentsRef = admin.database().ref('TournamentsInfo')
  if (keyIsCorrect) {
    tournamentsRef.orderByChild('startDate')
                  .limitToLast(1)
                  .once('value', snap => {
                    var lastType = '';
                    var lastStartDate;
                    snap.forEach((childSnapshot) => {
                      var value = childSnapshot.val();
                      lastStartDate = value.startDate;
                      lastType = value.type;
                    });
                    if (lastType) {
                      var startDateTimestamp = Date.parse(lastStartDate) + 604800000; //+7 days
                      var startDate = new Date(startDateTimestamp);
                      var tournamentsList = ['mintTask', 'colorbombTask', 'tokenTask', 'medalTask'];
                      var index = tournamentsList.indexOf(lastType) + 1;
                      if (index > tournamentsList.length - 1) {
                        index = 0;
                      }
                      var newTournamentRef = tournamentsRef.push();
                      newTournamentRef.set({
                        startDate: startDate.toISOString(),
                        type: tournamentsList[index]
                      });
                      res.status(200).send('OK');
                    } else {
                      res.status(404).send();
                    }
                  });
  } else {
    res.status(403).send();
  }
});

exports.addBattleScores = functions.https.onRequest((req, res) => {
  const battleId = String(req.query.battleId);
  const team = String(req.query.team);
  const scores = Number(req.query.scores);
  var ref = admin.database().ref('BattleData/' + battleId + '/TeamsScores/team' + team);
  ref.once('value', snapshot => {
    var currentTeamScores = snapshot.val();
    ref.set(currentTeamScores + scores);
    res.status(200).send('OK');
  }, errorObject => {
    res.status(500).send();
  })
})

exports.getPlayerPlaceInBattle = functions.https.onRequest((req, res) => {
  const battleId = String(req.query.battleId);
  const team = String(req.query.team);
  const playerScores = Number(req.query.playerScores);
  var ref = admin.database().ref('BattleData/' + battleId + '/' + team);
  ref.orderByChild("scores").startAt(playerScores).once('value', snapshot => {
    res.json({"place": snapshot.numChildren()});
  }, errorObject => {
    res.status(500).send();
  })
})

exports.getTeamsScoresInBattle = functions.https.onRequest((req, res) => {
  const battleId = String(req.query.battleId);
  var ref = admin.database().ref('BattleData/' + battleId + '/TeamsScores');
  ref.once('value', snapshot => {
    var teamsData = snapshot.val();
    res.json({'team1': teamsData.team1, 'team2': teamsData.team2});
  }, errorObject => {
    res.status(404).send();
  })
})


exports.messageToPlayer = functions.https.onRequest((req, res) => {
	if (req.method !== "POST") {
		res.status(405).send("Please send a POST request")
		return
	}
	if (req.get("content-type") !== "application/json") {
		res.status(400).send("Please send an application/json content");
		return
	}
	if (!req.body.deviceId) {
		res.status(400).send("Device ID is not specified");
		return
	}
	
	const now = new Date().toISOString().split(".")[0] + "Z"
	const ref = admin.database().ref("MessagesToPlayer/" + req.body.deviceId + "/Unread/" + now)
	ref.set(req.body.data, error => {
		if (error) {
			res.status(500).send()
		} else {
			res.status(200).send("Message is saved: " + now)
		}
	})
})


exports.progressToSupportAlias = functions.https.onRequest((req, res) => {
	const playerid = req.query.playerid
	const filename = req.query.filename
	const baseUrl = "https://firebasestorage.googleapis.com/v0/b/milamit-bakeacake.appspot.com/o/ProgressToSupport"
	const url = encodeURIComponent("/" + playerid + "/" + filename)
	res.redirect(baseUrl + url + "?alt=media")
})

//https://us-central1-milamit-bakeacake.cloudfunctions.net/progressToSupportAlias?playerid=3hcEfJp9TgTF707uFDlrkSlhti13&filename=2019-07-10__09_19_08
//https://firebasestorage.googleapis.com/v0/b/milamit-bakeacake.appspot.com/o/ProgressToSupport%2F3hcEfJp9TgTF707uFDlrkSlhti13%2F2019-07-10T09%3A19%3A08%2003%3A00?alt=media
