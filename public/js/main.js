jQuery(document).ready(function($) {

    $.cookieBar = function(options,val){
        var renewOnVisit = false;
        var expireDate = new Date();
        expireDate.setTime(expireDate.getTime()+(365*86400000));
        expireDate = expireDate.toGMTString();
        var cookieEntry = 'cookies-enabled={value}; expires='+expireDate+'; path=/';

        // The current cookie preference
        var i, cookieValue='', aCookie, aCookies=document.cookie.split('; ');
        for (i=0; i<aCookies.length; i++){
            aCookie = aCookies[i].split('=');
            if(aCookie[0]=='cookies-enabled'){
                cookieValue = aCookie[1];
            }
        }
        // Sets up default cookie preference if not already set
        if(cookieValue==''){
            cookieValue = 'enabled';
            document.cookie = cookieEntry.replace('{value}','enabled');
        } else if((cookieValue=='accepted') && renewOnVisit){
            document.cookie = cookieEntry.replace('{value}',cookieValue);
        }

        if(cookieValue=='enabled' || cookieValue==''){
            /*$('body').prepend('<div id="cookie-bar"><span class="cookie-msg">Information: This site uses cookies. By continuing to browse this site you are consenting to our use of cookies.</span><span class="cookie-btn"><i></i><i></i></span></div>');
            $('.navbar-fixed-top').css('top', $('#cookie-bar').outerHeight()-1);
            var h = $('#cookie-bar').outerHeight()+$('#main-nav').outerHeight()-2;
            $('body').css('padding-top', h);*/
        }

        $('#cookie-bar .cookie-btn').click(function(){
            document.cookie = cookieEntry.replace('{value}','accepted');
            var cookie_height = $('#cookie-bar').outerHeight();
            $('.navbar-fixed-top').animate({top:"-="+cookie_height}, 200);
            $('body').animate({"padding-top":"-="+cookie_height}, 200);
            $('#cookie-bar').slideUp(200,function(){
                $('#cookie-bar').remove();
            });
            return false;
        });
    };

    // Homepage slider
    if ($('.head-slider').length) {
        $('.head-slider').slick({
            dots: true,
            infinite: true,
            speed: 1000,
            autoplaySpeed: 6000,
            fade: true,
            autoplay: true,
            prevArrow: '<span class="left carousel-control"><span class="icon-prev"></span></span>',
            nextArrow: '<span class="right carousel-control"><span class="icon-next"></span></span>',
        });
    }

    setTimeout( function(){ $.cookieBar({}); }, 500);

    // Games page
    $('.banner, .main-banner', '.section-games').hover(
        function() { $('.btn', $(this).parent()).addClass('hover'); },
        function() { $('.btn', $(this).parent()).removeClass('hover'); }
    );

    // Main menu highlight
    $('a', '#main-nav').mouseup(function(){
        $(this).blur();
    });
	
	$('.navbar-nav li').click(function(){
		$('.navbar-nav li').removeClass('active');
		$(this).addClass('active');
	});

}(jQuery));
